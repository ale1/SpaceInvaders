﻿using System.Collections;
using UnityEngine;
using System;
using SpaceInvaders;
using UnityEngine.Networking;


namespace LeaderboardAPI
{

    [Serializable]
    public class TokenResponse {
        public string idToken;
        public string refreshToken;
        public User user;

        [Serializable]
        public struct User
        {
            public string id;
        }

        public bool Verify()
        {
            //do some verification that token is not malformed, expired or bad
            return true;
        }
    } 
    
    
    [Serializable]
    public class LeaderboardResponse
    {
        public Group group;

        [Serializable]
        public struct Group
        {
            public int week;
            public string start;
            public string end;
            public string tournamentId;
            public LeadersData[] players;

            [Serializable]
            public struct LeadersData
            {
                public string uid;
                public string name;
                public LeaderScore scores;
                
                [Serializable]
                public struct LeaderScore
                {
                    public int current;
                    public int past;
                }
            }
        }
    }
    

    public class APIController : Singleton<APIController>
    {
        [SerializeField] private bool useTestData = false;

        private string testToken =
            @"{
            ""idToken"":""idTokenValue"",
            ""refreshToken"":""refreshTokenValue"",
            ""user"": {
                ""id"": ""userid""
            }
        }";
        
        private string testLeaderboard =
            @"{
                ""group"": 
                {
                    ""week"": 6,
                    ""start"": ""2014-02-09T00:00:20Z"", 
                    ""end"": ""2060-02-19T00:00:20Z"", ""tournamentId"": ""someTourneyID"",
                    ""players"": 
                    [
                        { ""uid"": ""someUID1"", 
                            ""name"": ""jim"", 
                            ""scores"": 
                            {
                                ""current"": 10000,
                                ""past"": 500 
                             }
                        },
                        { ""uid"": ""someUID2"", 
                            ""name"": ""Pam"", 
                            ""scores"": 
                            {
                                ""current"": 8000,
                                ""past"": 0 
                             }
                        },
                        { ""uid"": ""someUID3"", 
                            ""name"": ""Dwight"", 
                            ""scores"": 
                            {
                                ""current"": 12000,
                                ""past"": 9994 
                             }
                        }    
                    ]
                }
            }";
        

        public Action<TokenResponse> OnTokenReady; // null = failure.
        public Action<LeaderboardResponse> OnRemoteDataReady; // null = failure
        public Action<bool> OnPostCompleted; //bool: true = success
        
        public void FetchToken(Action<TokenResponse> callback)
        {
            string url = "https://SyboSecretHQ.gov.dk/v1/auth/register";  
            OnTokenReady = callback;
            StartCoroutine(GetTokenFromAPI(url, HandleTokenSuccess, HandleTokenFail));
            
        }
   
        public void FetchLeaderboards(string idToken, string queryParam, Action<LeaderboardResponse> callback) //queryParam => country
        {
            string uri =  "https://SyboSecretHQ.gov.dk/v1/leaderboards?country={0}";
            string url = String.Format(uri,queryParam);
            OnRemoteDataReady = callback;
            StartCoroutine(GetDataFromAPI(idToken, url, HandleGETSuccess,HandleGETFail));
            
        }

        public void PostLastScore(string idToken, Action<bool> callback)
        {
            string url = "https://SyboSecretHQ.gov.dk/v1/leaderboards/submit";

            User user = SessionManager.GetInstance().CurrentUser;

            string tournament = user.lastScore.tournamentId;
            int points = user.lastScore.Points;

            if (idToken == null)
                return;
            
            OnPostCompleted = callback;
            StartCoroutine(PostDataCoroutine(
                idToken, 
                url, 
                tournament, 
                user.name, 
                points,
                HandlePOSTSuccess, 
                HandlePOSTFail));
           
        }


        IEnumerator GetTokenFromAPI(string url, Action<string> onSuccess, Action<string,long> onFailure)
        {
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.timeout = 3;

            using (www)
            {
                yield return www.SendWebRequest(); //waits

                if (www.isHttpError || www.isNetworkError)
                {
                    onFailure(www.error, www.responseCode);
                }
                else
                {
                    Debug.Log("receiving real data");
                    byte[] result = www.downloadHandler.data;
                    string dataJson = System.Text.Encoding.Default.GetString(result);
                    onSuccess(dataJson);
                }
            }
        }
        
        IEnumerator GetDataFromAPI(string idToken, string url, Action<string> onSuccess, Action<string,long> onFailure)
        {
            if (idToken == null)
            {
                onFailure("no id token",9999);
                yield break;
            }
                
            
            UnityWebRequest www = UnityWebRequest.Get(url);
            www.SetRequestHeader("idToken",idToken);
            www.timeout = 3;

            using (www)
            {
                yield return www.SendWebRequest(); //waits

                if (www.isHttpError || www.isNetworkError)
                {
                    onFailure(www.error, www.responseCode);
                }
                else
                {
                    Debug.Log("receiving real data");
                    byte[] result = www.downloadHandler.data;
                    string dataJson = System.Text.Encoding.Default.GetString(result);
                    onSuccess(dataJson);
                }
            }
        }

        IEnumerator PostDataCoroutine(string idToken, string url, string tournament, string name, int score, Action onSuccess, Action<string,long> onFailure)
        {
            WWWForm form = new WWWForm();
            form.AddField("tournament",tournament);
            form.AddField("name",name);
            form.AddField("score",score);
            
            UnityWebRequest www = UnityWebRequest.Post(url, form);
            www.SetRequestHeader("idToken",idToken);
            www.timeout = 3;

            using (www)
            {
                yield return www.SendWebRequest(); //waits
           
                if (www.isNetworkError || www.isHttpError)
                {
                    onFailure(www.error, www.responseCode);
                }
                else
                {
                    onSuccess();
                }
            }
        }

        private void HandleGETSuccess(string json)
        {
            //on success, parse returned json
            LeaderboardResponse data= ParseLeaderboardJson(json);
            OnRemoteDataReady?.Invoke(data);
            Debug.LogError("getSuccess");
        }

        private void HandleGETFail(string reason, long errorCode)
        {
            Debug.LogWarning("API get failed:" + reason);
            EvaluateErrorCode(errorCode, "GET");
            
            // Debug: On API failure, use fake data instead.
            
            if (!useTestData)
            {
                LeaderboardResponse data = null;
                OnRemoteDataReady?.Invoke(data);
            }
            else
            {
                LeaderboardResponse data = ParseLeaderboardJson(testLeaderboard);
                OnRemoteDataReady?.Invoke(data);
            }
        }

        private void HandlePOSTSuccess()
        {
            OnPostCompleted?.Invoke(true);
        }

        private void HandlePOSTFail(string reason, long errorCode = 999)
        {
            Debug.LogWarning("(API) POST to leaderboard failed:" + reason);
            EvaluateErrorCode(errorCode, "POST");
            OnPostCompleted?.Invoke(false);
        }


        private void HandleTokenSuccess(string json)
        {
            TokenResponse tokenResponse = ParseTokenJson(json);
            if(tokenResponse.Verify())
                OnTokenReady?.Invoke(tokenResponse);
            else
            {
                HandleTokenFail("failed SYBO in-house token verification",999);
            }
        }

        private void HandleTokenFail(string reason, long errorCode = 0)
        {
            Debug.LogWarning("(API) fetching token failure:" + reason);
            
            //Temporary, for debugging only.
            if (useTestData)
            {
                TokenResponse fakedata = ParseTokenJson(testToken);
                OnTokenReady?.Invoke(fakedata);
            }
            else
            {
                OnTokenReady?.Invoke(null);
            }
            
        }

        private LeaderboardResponse ParseLeaderboardJson(string json)
        {
            LeaderboardResponse LeaderboardResponse = JsonUtility.FromJson<LeaderboardResponse>(json);
            return LeaderboardResponse;
        }

        private TokenResponse ParseTokenJson(string json)
        {
            TokenResponse tokenResponse = JsonUtility.FromJson<TokenResponse>(json);
            //_refreshToken = tokenResponse.refreshToken;
            return tokenResponse;
        }


        private void EvaluateErrorCode(long errorCode, string origin)
        {
            return;
            
            switch (errorCode)
            {
                case 0:
                    Debug.LogError("no error code provided");
                    break;
                case 404:
                    Debug.LogError("bla bla bla");
                    break;
                case 9999:
                    Debug.LogError("e.g sybo custom error code");
                    break;
                    
                    
                //try to manage different types of errors depending on error code and source.  
                //e.g -> unathorized would try to recreate user data, forbidden -> would try to refresh token.,  bad request -> send log to devs, etc.  Bad request --> fire anaylitics event with request for forensics
                //not implemented because beyong scope of this game.
            }
            
        }
        
        
   
   
   
   
   
    }
}