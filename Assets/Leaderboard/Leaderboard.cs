﻿using System;
using System.Collections;
using System.Collections.Generic;
using LeaderboardAPI;
using UnityEngine;

namespace SpaceInvaders
{
    public class Leaderboard : MonoBehaviour
    {
        [SerializeField] private UI_ScoreEntry[] entries;
        
        private void Awake()
        {
            entries = GetComponentsInChildren<UI_ScoreEntry>(true);

        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnEnable()
        {
            LeaderboardManager.GetInstance().FetchScores();
        }

        public void Refresh(List<Score> scores)
        {
            for(int i=0; i < entries.Length; i++)
            {
                entries[i].Rank.text = (i + 1).ToString();
                if (scores.Count > i)
                {
                    entries[i].Name.text = scores[i].name;
                    entries[i].Score.text = scores[i].Points.ToString();
                }
                else //not enough entries to fill leaderboard
                {
                    entries[i].Name.text = "";
                    entries[i].Score.text = "";
                }
               
            }
        }
    }
}
