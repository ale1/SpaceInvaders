﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;

using LeaderboardAPI;
using UnityEditor;

namespace SpaceInvaders
{
    public class LeaderboardManager : Singleton<LeaderboardManager>
    {
        
        public List<Score> Scores = new List<Score>();

        public Leaderboard Leaderboard;
        
        // Start is called before the first frame update
        void Start()
        {
            LoadScores();// we load from local first, in case of slow connection, we can still have leaderboard data while we wait.
        }

        // Update is called once per frame
        void Update()
        {

        }
        
        public void FetchScores()
        {
            User currentUser = SessionManager.GetInstance().CurrentUser;
            
            if (currentUser.registered == false)
            {
                string idToken = currentUser.idToken;
                string country = currentUser.country;
                APIController.GetInstance().FetchLeaderboards(idToken, country, DataReceived);
            }
            else
            {
                LoadScores(); // no token, lets load from local
                Leaderboard.Refresh(Scores);
                
            }
         
        }
        

        public void DataReceived(LeaderboardResponse data)
        {
            
            if (data == null)
            {
                LoadScores(); // no api data, lets load from local
                Leaderboard.Refresh(Scores);
                return;
            }
            
            LeaderboardResponse.Group.LeadersData[] leaders = data.@group.players;

            Scores = new List<Score>();
            
            for (int i = 0; i < leaders.Length; i++)
            {
                Score score = new Score();
                score.name = leaders[i].name;
                score.SetTotal(leaders[i].scores.current);
                score.posted = true;  //if arriving through api, we know its on remote server;
                Scores.Add(score);
            }

            Scores = Scores.OrderByDescending(x => x.Points).ToList();

            SaveScores();
            Leaderboard.Refresh(Scores);
        }


        public bool isHighscore(int newpoints)
        {
            //check if score should be part of leaderboard
            if (Scores.Count < 10)
                return true;   //not enough scores to fill leaderboard, so new score is always a highscore.
            
            for(int i = Scores.Count -1; i < 0; i--)
            {
                if (newpoints > Scores[i].Points)
                    return true;
            }

            return false;
        }

        public void AddLastScore()
        {
            User user = SessionManager.GetInstance().CurrentUser;
            
            Scores.Add(user.lastScore);
            
            Scores = Scores.OrderByDescending(x => x.Points).ToList();
            while (Scores.Count > 10)
            {
                Scores.RemoveAt(Scores.Count - 1);
            }
            
            SaveScores(); //save locally

            //now send to remote, 
            string idToken = SessionManager.GetInstance().CurrentUser.idToken;
            APIController.GetInstance().PostLastScore(idToken, OnPostCompleted);
            user.lastScore.posted = true; // todo: only do this if post has succeeded.
        }

        public void OnPostCompleted(bool success)
        {
            if (success)
            {
                FetchScores();   
            }
            else
            {
                //only load scores locally
                LoadScores(); 
                if(Leaderboard.isActiveAndEnabled)
                    Leaderboard.Refresh(Scores);
            }
        }

        private void SaveScores()
        {
            string savePath = Application.persistentDataPath + "/leaderboard.data";   
            
            FileStream fs = new FileStream(savePath, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, Scores);
            fs.Close();
        }

        private void LoadScores()
        {
            string saveFile = Application.persistentDataPath + "/leaderboard.data";
            
            if (File.Exists(saveFile))
            {
                using (Stream stream = File.Open(saveFile, FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    Scores = (List<Score>)bformatter.Deserialize(stream);
                }
            }
        }
        
        [ContextMenu("Erase Saved Scores")]
        void EraseDB()
        {
            Scores = new List<Score>();
            SaveScores();
        }
    }
    
    [Serializable]
    public class Score
    {
        public string name = "No Name";
        public int Points { get; private set; }
        public string tournamentId = "??";  //where is tounramentID data obtained?? is it the same as country?
        public bool posted = false;


        public void SetTotal(int val)
        {
            this.Points = val;
        }
        public void Add(int points)
        {
            this.Points += points;
        }
    }
    
}
