﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BunkerPiece : MonoBehaviour
{
    
    private int _health = 3;

    private Color[] colors = new Color[]
        {new Color(0.5f, 0.5f, 0.5f), new Color(0.75f, 0.75f, 0.75f), new Color(1f, 1f, 1f)};

    BoxCollider2D _coll;
    SpriteRenderer _rend;
    

    // Start is called before the first frame update
    void Awake()
    {
        _coll = GetComponent<BoxCollider2D>();
        _rend = GetComponent<SpriteRenderer>();

    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void OnTriggerEnter2D(Collider2D other)
    {
        Invader invader = other.GetComponent<Invader>();
        if (invader)
        {
            Damage(_health);
            return;
        }

        Laser laser = other.GetComponent<Laser>();
        if (laser && laser.CanHitBunker)
        {
            Damage();
            laser.gameObject.SetActive(false);
        }
    }
    
    private void Damage(int amount = 1)
    {
        _health -= amount;
        if (_health <= 0)
        {
            _rend.enabled = false;
            _coll.enabled = false;
        }
        else
        {
            Colorize();
        }
    }

    private void OnEnable()
    {
        Reset();
    }

    public void Reset()
    {
        _health = 3;
        _rend.enabled = true;
        _coll.enabled = true;
        
        Colorize();
    }

    private void Colorize()
    {
        int index = Mathf.Clamp(_health - 1,0, colors.Length - 1);
        _rend.color = colors[index];
    }
    
    
    
}
