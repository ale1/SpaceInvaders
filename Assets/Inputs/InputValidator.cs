﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using SpaceInvaders;
using UnityEngine.Events;

public class InputValidator : MonoBehaviour
{
    public Button submitButton;
    public GameObject warningText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ValidateString(string val)
    {
        bool ok = true;
        Regex regex = new Regex(@"^[a-zA-Z0-9]*$");
        Match match = regex.Match(val);
        if (!match.Success)
        {
            ok = false;
        }

        warningText.SetActive(!ok);
        submitButton.interactable = ok;

        if (ok)
        {
            SessionManager.GetInstance().SetUserName(val);
        }
    }
    
}
