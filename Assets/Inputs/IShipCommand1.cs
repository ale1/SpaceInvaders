﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    /// <summary>
    /// command for firing 
    /// </summary>
    public interface IFormCommand
    {
        void SubmitForm();
        void CancelForm();
    }


