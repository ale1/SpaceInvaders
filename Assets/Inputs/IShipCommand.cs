﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    /// <summary>
    /// command for firing 
    /// </summary>
    public interface IShipCommand
    {
        void ShootInput();
        void MoveInput(float direction);

        void TouchInput(Vector2 screenPos);
    }


