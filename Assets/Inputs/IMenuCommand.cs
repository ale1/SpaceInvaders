﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    /// <summary>
    /// command when user presses enter
    /// </summary>
    public interface IMenuCommand
    {
        void SelectInput();
     
    }

