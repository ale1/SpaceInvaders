﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    /// <summary>
    /// class for handling inputs through a command pattern
    /// </summary>
    public class InputHandler : Singleton<InputHandler> 
    {
        public List<IShipCommand> ShipCommandSubscribers = new List<IShipCommand>();
        public List<IMenuCommand> MenuCommandSubscribers = new List<IMenuCommand>();
        public List<IPauseCommand> PauseCommandSubscribers = new List<IPauseCommand>();
        public List<IFormCommand> FormCommandSubscribers = new List<IFormCommand>();


        public void SubmitForm()
        {
            for(int i = FormCommandSubscribers.Count - 1; i >= 0; i--)
            {
                FormCommandSubscribers[i].SubmitForm();
            }
        }

        public void CancelForm()
        {
            for(int i = FormCommandSubscribers.Count - 1; i >= 0; i--)
            {
                FormCommandSubscribers[i].CancelForm();
            }
        }
        
        
        void Update()
        {
            //touch input for mobile use
            if (Application.isMobilePlatform)
            {
                if (Input.touchCount > 0)
                {
                    Touch t = Input.GetTouch(0);
                    for (int i = ShipCommandSubscribers.Count - 1; i >= 0; i--)
                    {
                            ShipCommandSubscribers[i].TouchInput(t.position);
                    }
                }
            }
          




#if UNITY_EDITOR || UNITY_STANDALONE
            
            if (Input.GetKeyDown(KeyCode.Return))
            {
                for(int i = MenuCommandSubscribers.Count - 1; i >= 0; i--)
                {
                    MenuCommandSubscribers[i].SelectInput();
                }
            }
            
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                
                for(int i = ShipCommandSubscribers.Count - 1; i >= 0; i--)
                {
                    ShipCommandSubscribers[i].ShootInput();
                }
                
            }

            float axisDirection = Input.GetAxisRaw("Horizontal");
            if (axisDirection != 0)
            {
                for(int i = ShipCommandSubscribers.Count - 1; i >= 0; i--)
                {
                    ShipCommandSubscribers[i].MoveInput(axisDirection);
                }
            }
            
            if (Input.GetKeyDown(KeyCode.P))
            {
                for(int i = PauseCommandSubscribers.Count - 1; i >= 0; i--)
                {
                    PauseCommandSubscribers[i].PauseInput();
                }
            }
            
            #endif
           
        }
    }

