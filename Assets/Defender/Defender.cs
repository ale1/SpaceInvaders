﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.WebCam;

public class Defender : MonoBehaviour, IShipCommand
{
    public int StartLives = 3;
    
    private int _currentLives;
    
    public LifeMeter lifeMeter;
    
    public float Speed = 1f;
    public float fireRate = 0.5f;

    public Transform LeftMarker;
    public Transform RightMarker;

    private Vector3 startPos;

    public Laser Laser;

    public Action OnLoseCondition;

    private float size;

    public Action OnDeath;

    public bool Dead { get; private set; }


    private void Awake()
    {
        startPos = this.transform.position;
    }

    void Start()
    {
        InputHandler.GetInstance().ShipCommandSubscribers.Add(this);
        size = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TouchInput(Vector2 pos)
    {
        if(pos.x < transform.position.x)
            MoveInput(-1);
        else
        {
            MoveInput(direction: 1);
        }

    }

    public void MoveInput(float direction)
    {
        if (Dead)
            return;

        Vector3 pos = transform.position;
        Vector3 targetPos = new Vector3(pos.x + Speed * direction * Time.deltaTime, pos.y, pos.z);

        if (targetPos.x - size / 2 < LeftMarker.position.x ||
            targetPos.x + size / 2 > RightMarker.position.x)
        {
            Debug.Log("at edge, cant move");

        }
        else
        {
            transform.position = targetPos;
        }
    }

    public void ShootInput()
    {
        if (Dead || Laser.isActiveAndEnabled)
            return;
        else
        {
            Laser.transform.position = this.transform.position;
            Laser.gameObject.SetActive(true);

        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (Dead)
            return;

        Laser laser = other.GetComponent<Laser>();
        if (laser && laser.CanHitDefender && laser.isActiveAndEnabled)
        {
            Laser.gameObject.SetActive(false);
            _currentLives--;
            lifeMeter.Refresh(_currentLives);
            //GetComponent<Animator>().SetTrigger("Explode"); //todo
            
            if(_currentLives <= 0)
                Die();
            return;
        }

        Invader invader = other.GetComponent<Invader>();
        if (invader) //direct collision with invader
            Die();
    }

    private void OnEnable()
    {
        transform.position = startPos;
        Laser.gameObject.SetActive(false);
        _currentLives = StartLives;
        lifeMeter.Refresh(_currentLives);
        Dead = false;

        //if playing on mobile device, shoot automatically.
        if(Application.isMobilePlatform)
            InvokeRepeating("ShootInput", 1f, fireRate);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }


    private void Die()
    {
        Dead = true;
        OnDeath?.Invoke();
    }
};
