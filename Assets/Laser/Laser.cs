﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    [Range(3,12)]
    public float speed;

    [Header("1 for up, -1 for down")]
    public int _direction = 1;
    
    public bool CanHitInvaders;
    public bool CanHitDefender;
    public bool CanHitBunker;

    private AudioSource _audioSource;
    
    private void Awake()
    {
      
    }

    // Start is called before the first frame update
    void Start()
    {
       #if UNITY_EDITOR
        if(_direction != 1 && _direction != -1)
            Debug.LogError("invalid direction value");
        #endif

        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(this.transform.position);
        bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
        
        if(onScreen)
            transform.Translate(0,speed * Time.deltaTime * _direction,0);
        else
        {
            this.gameObject.SetActive(false);
        }
    }

    public void OnEnable()
    {
        if(_audioSource && _audioSource.clip != null)
            _audioSource.Play();
    }

    public void OnDisable()
    {
        this.transform.position = transform.parent.position;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
    }
}
