﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeMeter : MonoBehaviour
{

    public SpriteRenderer _renderer;
    
    public Sprite[] numbers;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    public void Refresh(int lives)
    {
        _renderer.sprite = numbers[lives];
    }
}
