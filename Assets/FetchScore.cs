﻿using System;
using System.Collections;
using System.Collections.Generic;
using SpaceInvaders;
using UnityEngine;
using UnityEngine.UI;

public class FetchScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        GetComponent<Text>().text = SessionManager.GetInstance().CurrentUser.lastScore.Points.ToString();
    }
}
