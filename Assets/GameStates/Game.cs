﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;
using SpaceInvaders;
using UnityEngine.UI;


/// <summary>
/// Manages this specific game instance.  
/// </summary>
public class Game : MonoBehaviour,  IPauseCommand
{
    [SerializeField] private InvaderController InvaderController;
    
    [Range(30,150)]
    [Header(" timer that affects bonus points awarded")]
    public int BonusTime;

    [Header("base points for calculating bonus points")]
    public int BonusBase = 200;
    

    
    public Text Scoresign;
    
    public Action<bool, bool> OnEndGame;

    public GameObject PauseSign;

    public Score currentScore { get; private set; }
    
    public bool paused;

    private int timer;

    [Header("Debug use only")]
    public bool cheat = false; // for debug only.

    private User _user;
    
    public void Start()
    {
        GetComponentInChildren<Defender>().OnDeath += OnLoseGame;
        InvaderController.OnInvadersDead += OnWinGame;
        InvaderController.OnPointsEarned += i => { currentScore.Add(i);
            Scoresign.text = currentScore.Points.ToString();
        };
        GetComponentInChildren<MysteryShip>().OnPointsEarned += i => { currentScore.Add(i);
            Scoresign.text = currentScore.Points.ToString();
        };
    }

    public void OnEnable()
    {
        _user = SessionManager.GetInstance().CurrentUser;
        Scoresign.text = "0";
        timer = BonusTime;
        InputHandler.GetInstance().PauseCommandSubscribers.Add(this);
        paused = false;
        currentScore = new Score();
        
        InvokeRepeating(nameof(countdown),0f,1f); //hehe
    }

    public void OnDisable()
    {
        InputHandler.GetInstance().PauseCommandSubscribers.Remove(this);
    }

    private void countdown()
    {
        timer++;
    }

    public void OnLoseGame()
    {
        _user.lastScore = currentScore;
        
        if(!cheat)
            OnEndGame(false, false);
        else
        {
            OnEndGame(true, true);
        }
    }

    public void OnWinGame()
    {
        int bonusPoints = BonusBase * (BonusTime / timer);
        currentScore.Add(bonusPoints);
        _user.lastScore = currentScore;
        bool highscore = LeaderboardManager.GetInstance().isHighscore(currentScore.Points);
        OnEndGame(true, highscore);
    }
    
    #region InputHandling
    
    public void PauseInput()
    {
        paused = !paused;
        Time.timeScale = paused ? 0 : 1;
        Debug.Log("Game paused: " + paused);
        PauseSign.SetActive(paused);
        
    }
    #endregion
    

}
