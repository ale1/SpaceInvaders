﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvaders;

public class GameStateMenu : GameState, IMenuCommand
{
        public GameStateMenu(SessionManager session) : base(session)
        {
               
       
        }

        public override void OnStateEnter()
        {       
                InputHandler.GetInstance().MenuCommandSubscribers.Add(this);
                _session.Canvas.gameObject.SetActive(true);
                    
        }

        public override void OnStateExit()
        {
                _session.MainMenuWindow.SetActive(true);   
                _session.Canvas.gameObject.SetActive(false);
                InputHandler.GetInstance().MenuCommandSubscribers.Remove(this);
        }

        public override void OnUpdate()
        {
        }

        #region InputHandling

        public void SelectInput()
        {
                _session.SetState(new GameStatePlaying(_session));
        }

        #endregion
}
