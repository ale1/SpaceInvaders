﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvaders;

public class GameStateHighscore : GameState, IFormCommand
{
    private Score _highscore;
    
    public GameStateHighscore(SessionManager sessionManager) : base(sessionManager) {
        InputHandler.GetInstance().FormCommandSubscribers.Add(this);
    }


    public override void OnStateEnter()
    {
        _session.Canvas.gameObject.SetActive(true);
        _session.HighScoreWindow.SetActive(true);
    }

    public void SubmitForm()
    {
        _session.leaderboardManager.AddLastScore();
        _session.SetState(new GameStateMenu(_session));
    }

    public void CancelForm()
    {
        _session.SetState(new GameStateMenu(_session));
    }

    public override void OnStateExit()
    {
        InputHandler.GetInstance().FormCommandSubscribers.Remove(this);
        _session.HighScoreWindow.SetActive(false);
    }

    public override void OnUpdate()
    {
       
    }
}
