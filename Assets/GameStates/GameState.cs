﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvaders;

public abstract class GameState 
{
    protected SessionManager _session;

    public GameState(SessionManager session) {
        this._session = session;
    }
    
    public abstract void OnStateEnter();
    public abstract void OnStateExit();
    public abstract void OnUpdate();
    
    


}
