﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SpaceInvaders;

public class GameStatePlaying : GameState
{

    private bool loserCanSubmitScore = true; //for debug only. todo: remove
    
    public GameStatePlaying(SessionManager session) : base(session) {
         
    }

    public override void OnStateEnter()
    {
        _session.Game.gameObject.SetActive(true);
        _session.Game.OnEndGame += EndGame;
    }

    public override void OnStateExit() {
        _session.Game.OnEndGame -= EndGame;
        _session.Game.gameObject.SetActive(false);
    }

    public override void OnUpdate() {
        
    }
    
    private void EndGame(bool won, bool highscore)
    {
        //Debug.LogError("Game: " + (won ? "won" : "lost"));
        Debug.Log("highscore:" + highscore);
        
        if ((won || loserCanSubmitScore) && highscore)
        {
            _session.SetState(new GameStateHighscore(_session));
        }
        else
        {
            _session.SetState(new GameStateMenu(_session));
        }
            
    }
    
 
}
