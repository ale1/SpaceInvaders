﻿using System;
using System.Collections;
using System.Collections.Generic;
using LeaderboardAPI;
using UnityEngine;

namespace SpaceInvaders
{
    public class SessionManager : Singleton<SessionManager>
    {
        public User CurrentUser { get; private set; }
        public LeaderboardManager leaderboardManager;
        
        public Canvas Canvas;
        public GameObject MainMenuWindow;
        public GameObject HighScoreWindow;
        
        public Game Game;

        private bool _transitioning = false;

        public GameState currentState;


        void Awake()
        {
            CurrentUser = new User();
        }

        public void SetUserName(string val = "")
        {
            CurrentUser.name = val;
            CurrentUser.lastScore.name = val;
        }

        public void SetState(GameState state)
        {

            _transitioning = true;

            if (currentState != null)
                currentState.OnStateExit();

            currentState = state;
            gameObject.name = "GameState:" + currentState.GetType().Name;

            if (currentState != null)
                currentState.OnStateEnter();
        }

        private void Start()
        {
            CurrentUser = new User {country = "dk"};  //todo: api doesnt provide user country value, so hardcoding...
            LeaderboardAPI.APIController.GetInstance().FetchToken(TokenReceived);
        }


        private void TokenReceived(TokenResponse data)
        {
            if (data == null)
                CurrentUser.idToken = null;
            else
            {
                CurrentUser.idToken = data.idToken;
                //we got a new token, lets fetch some remote leaderboards
                leaderboardManager.FetchScores();
            }
        }
      

        private void OnEnable()
        {
            SetState(new GameStateMenu(this));
        }

        private void OnDisable()
        {

        }

        void Update()
        {
            if (_transitioning)
            {
                //if transitioning between states, skip a frame on update cycle, since race condition can result in prior state OnUpdate() being called on first cycle of new state
                _transitioning = false;
                return;
            }

            currentState.OnUpdate();
        }

        public void StartGame() //used by UI button
        {
            SetState(new GameStatePlaying(this));
        }
        
    }
    
}
