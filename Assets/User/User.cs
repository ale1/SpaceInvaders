﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using LeaderboardAPI;


namespace SpaceInvaders
{
    [Serializable]
    public class User
    {
        public bool registered => (idToken != null);
        public string country = "dk"; //where to get this value?
        public string idToken = null;
        public string refreshToken;
        public string name = "noName";
        public Score lastScore = new Score();  
    }

}
