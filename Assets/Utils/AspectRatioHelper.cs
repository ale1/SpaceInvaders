﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class AspectRatioHelper : MonoBehaviour
{
    [Tooltip("Set the desired aspect ratio.  I.e 16f/9f")]
    public float targetAspect = 5f/3f;
    
    
    public UnityEvent onOrientationChange = new UnityEvent();
    public UnityEvent onResolutionChange = new UnityEvent();

    Vector2 _lastResolution = Vector2.zero;
    ScreenOrientation _lastOrientation;

    private void Awake()
    {
        _lastOrientation = Screen.orientation;
        _lastResolution.x = Screen.width;
        _lastResolution.y = Screen.height;
    }


    void Start ()
    {
        EnforceAspectRatio();

    }

    void Update()
    {
        if (Application.isMobilePlatform)
        {
            if (Screen.orientation != _lastOrientation)
                OrientationChanged();
        }
        else
        {
            //resolution of mobile devices should stay the same always, right?
            // so this check should only happen in non-mobiles
            if (!Mathf.Approximately(Screen.width, _lastResolution.x) || !Mathf.Approximately(Screen.height, _lastResolution.y))
                ResolutionChanged();
        }
    }
    
    private void EnforceAspectRatio()
    {
        // determine the game window's current aspect ratio
        float windowaspect = (float)Screen.width / (float)Screen.height;

        // current viewport height should be scaled by this amount
        float scaleheight = windowaspect / targetAspect;

        // obtain camera component so we can modify its viewport
        Camera camera = GetComponent<Camera>();

       
        if (scaleheight < 1.0f)
        {  
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;
        
            camera.rect = rect;
        }
        else 
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            camera.rect = rect;
        }
    }
    
    void OrientationChanged()
    {
        _lastOrientation = Screen.orientation;
        _lastResolution.x = Screen.width;
        _lastResolution.y = Screen.height;

        EnforceAspectRatio();
        onOrientationChange?.Invoke();
    }

    void ResolutionChanged()
    {
        if (Mathf.Approximately(_lastResolution.x, Screen.width) && Mathf.Approximately(_lastResolution.y, Screen.height))
            return;

        //Debug.Log("Resolution changed from " + lastResolution + " to (" + Screen.width + ", " + Screen.height + ") at " + Time.time);

        _lastResolution.x = Screen.width;
        _lastResolution.y = Screen.height;

        EnforceAspectRatio();
        onResolutionChange?.Invoke();
    }
    
}
