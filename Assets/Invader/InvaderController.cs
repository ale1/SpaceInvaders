﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using SpaceInvaders;
using Unity.Mathematics;
using UnityEngine.Animations;
using Random = System.Random;

public class InvaderController : MonoBehaviour
{
    public Action OnInvadersDead;
    public Action<int> OnPointsEarned;

    public Transform leftMarker;
    public Transform rightMarker;
    
    
    private Invader[] _invaders;
    private float timer;

    private const float InitialDelay = 0.9f;
    public float movementRate = 0.6f;

    private void Awake()
    {
        _invaders = GetComponentsInChildren<Invader>();
    }

    void Start()
    {
        int col = 0;
        int row = 0;
        
        //Invader Setup
        foreach (var invader in _invaders)
        {
            if (col > 10)
            {
                row++;
                col = 0;
            }

            invader.coord = new Vector2Int(row, col);
            invader.gameObject.name = "invader--"+col+"-"+row;
            invader.canShoot = (row == 0);
            invader.leftMarker = leftMarker;
            invader.rightMarker = rightMarker;
            
            col++;
            invader.OnDeath += HandleInvaderDeath;
            
        }
        
    }

    // Update is called once per frame
    void Update()
    {
  
    }

    public void OnEnable()
    {
        foreach (var invader in _invaders)
        {
            invader.Setup(InitialDelay, movementRate);
        }
        InvokeRepeating(nameof(ExecuteMoves),InitialDelay + movementRate / 2, repeatRate:movementRate); //note: could use coroutines and yield return null for exact frame delays instead of timers. 
    }

    public void OnDisable()
    {
        CancelInvoke();
    }

    public void ExecuteMoves()
    {
        bool vertical = false;
        foreach (var invader in _invaders)
        {
            if (invader.blocked)
            {
                vertical = true;
                break;
            }
        }
        
        foreach (var invader in _invaders)
        {
            invader.Move(vertical);
        }      
    }
    

    public bool InvadersDead()  //check win condition
    {
        foreach (var invader in _invaders)
        {
            if (!invader.Dead)
                return false;
        }
        return true;
    }
    
    public void HandleInvaderDeath(Vector2Int pos, int color, bool assignShooter, int scoreValue)
    {
        //add score
        OnPointsEarned?.Invoke(scoreValue);
        
        foreach (var invader in _invaders)
        {
            int x = invader.coord.x;
            int y = invader.coord.y;
            
            if (assignShooter) //if shooter is dying, find replacement shooter in same column
            {
                if (y == pos.y && !invader.Dead)
                {
                    invader.canShoot = true;
                    assignShooter = false;
                }
            }

            if (!invader.Dead && invader.ColorIndex == color)
            {
                if (x == pos.x && Math.Abs(y - pos.y) == 1 || //horizontal neighbour
                    (y == pos.y && Math.Abs(x - pos.x) == 1)) //vertical neighbour
                {
                    invader.Die();
                }
            }
        }
        
        if(InvadersDead())  //if all invaders are dead, 
            OnInvadersDead.Invoke();
    }
}
