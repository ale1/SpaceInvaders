﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class MysteryShip : MonoBehaviour
{

    [SerializeField] private Transform leftMarker;
    [SerializeField] private Transform rightMarker;
    public float repeatRate;

    public Action<int> OnPointsEarned;
    
    public int scoreValue = 50;
    
    public float speed;

    private BoxCollider2D _collider;
    private bool _moving = false;

    private SpriteRenderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();
        ResetPos();

    }

    // Update is called once per frame
    void Update()
    {
        if (_moving)
        {
            float moveDelta = Mathf.Min(speed * Time.deltaTime);
            transform.position = Vector3.MoveTowards(transform.position, rightMarker.transform.position, moveDelta);
        }
    }

    private void OnEnable()
    {
        InvokeRepeating("Travel", repeatRate, repeatRate);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }


    private void Travel()
    {
        if (!Mathf.Approximately(transform.position.x, rightMarker.transform.position.x))
            return; //hasnt finished prev move.
        
        _renderer.enabled = true;
        transform.position = leftMarker.transform.position;
        _moving = true;
    }
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        Laser laser = other.GetComponent<Laser>();
        if (laser && laser.CanHitInvaders)
        {
            laser.gameObject.SetActive(false);
            Die();
        }
    }

    private void Die()
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(this.transform.position);
        float xscreen = screenPoint.x;
        float ratio;
        if (screenPoint.x >= 0.5)
        {
            ratio = (1 - xscreen) / 0.5f;
        }
        else
        {
            ratio = xscreen / 0.5f;
        }

        //add score 
        Debug.Log("mystery hit for:" + Math.Round(scoreValue * ratio));
        OnPointsEarned?.Invoke((int) Math.Round(scoreValue * ratio));
        
        _renderer.enabled = false;
        ResetPos();
        GetComponent<BoxCollider2D>().enabled = false;  //todo refactor
    }


    private void ResetPos()
    {
        transform.position = rightMarker.transform.position;
    }
}
