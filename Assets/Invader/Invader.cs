﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class Invader : MonoBehaviour
{
    
    public int scorevalue;
    public float firingRateMin;
    public float firingRateMax;
    public Color[] invaderColors = new[] {Color.blue, Color.green, Color.yellow, Color.red, Color.cyan,};
    
    [HideInInspector] public Transform leftMarker;
    [HideInInspector] public Transform rightMarker;
    [HideInInspector] public bool blocked;  //true when about to hit edge on next move
    [HideInInspector] public bool canShoot;
    
    readonly int _moveHash = Animator.StringToHash("Move");
    
    public int ColorIndex { get; private set; }
    public float HorDelta { get; private set; }
    public float VertDelta { get; private set; }
    
    private int _direction = 1;  //1 for right, -1 for left
    private float _size;
    private Vector3 _nextPos;
   

    public Action<Vector2Int,int, bool,int> OnDeath;
    public bool Dead { get; private set; }

    public Vector2Int coord;

    private SpriteRenderer _renderer;
    private BoxCollider2D _collider;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();
    }


    void Start()
    {
        HorDelta = (rightMarker.transform.position.x - leftMarker.transform.position.x) / 28;
        Vector2 rendSize = _renderer.bounds.size;
        _size = rendSize.x;  
        VertDelta = _renderer.bounds.size.y / 2;
        //dynamically resize collider to match sprite size
        _collider.size = rendSize;

    }

    public void Setup(float initialDelay, float rate)
    {
        InvokeRepeating(nameof(Invader.CalcMove),initialDelay, rate);
    }

    private void OnEnable()
    {
        ColorIndex = Random.Range(0, invaderColors.Length);
        Color randomColor = invaderColors[ColorIndex];
        _renderer.color = randomColor;
        InvokeRepeating(nameof(Shoot),Random.Range(firingRateMin,firingRateMax),Random.Range(firingRateMin,firingRateMax));
        Dead = false;
        _renderer.enabled = true;
        _collider.enabled = true;
    }

    private void OnDisable()
    {
        CancelInvoke();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CalcMove()
    {
        Vector3 pos = transform.position;
        _nextPos = new Vector3(pos.x + HorDelta * _direction, pos.y, pos.z);
        float checkPos = _nextPos.x + _size / 2 * _direction;

        if (Dead)
            return;
        
        if (_direction == 1 ? checkPos > rightMarker.transform.position.x : checkPos < leftMarker.transform.position.x)
        {
            blocked = true;
        }
    }


    public void Move(bool vertical)
    {
        if(vertical)
            MoveVertical();
        else
        {
            MoveHorizontal();
        }
    }

    private void MoveHorizontal()
    {
        transform.position = _nextPos;
        
        Animator anim = GetComponent<Animator>(); // todo refactor
        if (anim)
        {
            anim.SetTrigger(_moveHash);

        }
    }

    private void MoveVertical()
    {
        transform.Translate(0, -VertDelta, 0);
        _direction *= -1;
        blocked = false;
    }
    
    #region CollisionDetection
    public void OnTriggerEnter2D(Collider2D other)
    {
        Laser laser = other.GetComponent<Laser>();
        if (laser && laser.CanHitInvaders)
        {
            laser.gameObject.SetActive(false);
            Die();
        }
    }
    #endregion

    public void Die()
    {
        Dead = true;
        OnDeath?.Invoke(coord,ColorIndex,canShoot,scorevalue);
        canShoot = false;
        _renderer.enabled = false;
        _collider.enabled = false;  
        //todo: SFX;
    }

    public void Shoot()
    {
        if (!canShoot)
            return;

        GameObject laserGO = ObjectPool.SharedInstance.GetFromPool();
        if (laserGO != null)
        {
            laserGO.transform.position = this.transform.position;
            laserGO.SetActive(true);
        }
        else
        {
            Debug.LogError("something went wrong when fetching from object pool");
        }
    }
    

  
}
